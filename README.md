# Raspberry Pi RTC Utilities #

A collection of utilities to assist with the use of various _real-time clock_ modules on Raspberry Pi hardware.


## Supported RTC Hardware ##

While the goal is to support a variety of real-time clock modules, in reality, not all modules are able to be supported. The more
common modules will be supported first.

### **Common Modules** _(likely to be supported sooner)_ ###

Below is a list of the more commonly available/used RTC modules, which are likely to be supported first.

* [DS1307][1]	- A common, inexpensive RTC module from **Dallas** _(now **[MAXIM][101]**)_, uses **I²C** for connectivity.
* [DS3231][2]	- Another common, relatively inexpensive module from **Dallas** _(now **[MAXIM][101]**)_, uses **I²C** for connectivity.
  * [DS3231M][2a]	- A derivative of the above [**DS3231** module][2], uses **I²C** for connectivity.


### RTC Hardware Interfaces ###

RTC modules generally are connected to the host _(in this case, the **Raspberry Pi**)_ via a _serial-based connection_. Generally, this
connection is **I²C**, however, there are a handful of RTC modules out there that utilize the **SPI** bus for host connectivity.

In _nearly every case, **I²C is preferred**_. This is due to the fact that **I²C** utilizes fewer hardware pins and resources, and RTC
modules aren't demanding in terms of connection bandwidth or resources required, making **I²C** preferred.


#### DS1307 ####

**NOTE:** _The below description and list of key features are sourced from the [MAXIM Product Page][1]._

##### Description #####

The **DS1307** serial real-time clock (RTC) is a low-power, full binary-coded decimal (BCD) clock/calendar plus 56 bytes of NV SRAM. Address and data are transferred serially through an _**I²C**, bidirectional bus_. The clock/calendar provides seconds, minutes, hours, day, date, month, and year information. The end of the month date is automatically adjusted for months with fewer than 31 days, including corrections for leap year. The clock operates in either the 24-hour or 12-hour format with AM/PM indicator. The DS1307 has a built-in power-sense circuit that detects power failures and automatically switches to the backup supply. Timekeeping operation continues while the part operates from the backup supply.


##### Key Features #####

* Completely Manages All Timekeeping Functions
    * Real-Time Clock Counts Seconds, Minutes, Hours, Date of the Month, Month, Day of the Week, and Year with Leap-Year Compensation Valid Up to 2100
    * 56-Byte, Battery-Backed, General-Purpose RAM with Unlimited Writes
    * Programmable Square-Wave Output Signal
* Simple Serial Port Interfaces to Most Microcontrollers
    * I²C Serial Interface
* Low Power Operation Extends Battery Backup Run Time
    * Consumes Less than 500nA in Battery-Backup Mode with Oscillator Running
    * Automatic Power-Fail Detect and Switch Circuitry
* 8-Pin DIP and 8-Pin SO Minimizes Required Space
* Optional Industrial Temperature Range: -40°C to +85°C Supports Operation in a Wide Range of Applications
* Underwriters Laboratories® (UL) Recognized


##### Utilities #####



#### DS3231 ####



##### DS3231M #####







[1]:	<https://www.maximintegrated.com/en/products/digital/real-time-clocks/DS1307.html>
[2]:	<https://www.maximintegrated.com/en/products/digital/real-time-clocks/DS3231.html>
[2a]:	<https://www.maximintegrated.com/en/products/digital/real-time-clocks/DS3231M.html>
[101]:	<https://www.maximintegrated.com>